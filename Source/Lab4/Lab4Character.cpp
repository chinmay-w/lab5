// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ALab4Character::ALab4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	interactionLength = 3.f;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm Component"));
	SpringArmComponent->SetUsingAbsoluteRotation(true);
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->TargetArmLength = 400.0f;
	SpringArmComponent->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	SpringArmComponent->bDoCollisionTest = false;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;

	
}

// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ALab4Character::CanPerformAction(ECharacterActionStateEnum updatedAction) {
	switch (CharacterActionState) {
	case ECharacterActionStateEnum::IDLE:
		return true;
		break;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction != ECharacterActionStateEnum::INTERACT) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE) {
			return true;
		}
		break;

	case ECharacterActionStateEnum::INTERACT:
		return false;
		break;
	}

	return false;
}

void ALab4Character::ApplyMovementForward(float scale) {
	AddMovementInput(GetActorForwardVector(), scale);
}

void ALab4Character::ApplyMovementRight(float scale) {
	AddMovementInput(GetActorRightVector(), scale);
}

void ALab4Character::BeginInteract() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Interaction started"));
		GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ALab4Character::EndInteract, interactionLength, false);
	}
}

void ALab4Character::EndInteract() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Interaction ended"));
		UpdateActionState(ECharacterActionStateEnum::IDLE);
	}
}

void ALab4Character::UpdateActionState(ECharacterActionStateEnum newAction) {
	if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE) {
		if (FMath::Abs(GetVelocity().Size()) <= 0.01f) {
			CharacterActionState = ECharacterActionStateEnum::IDLE;
		}
		else {
			CharacterActionState = ECharacterActionStateEnum::MOVE;
		}
	}
	else {
		CharacterActionState = newAction;
	}
}