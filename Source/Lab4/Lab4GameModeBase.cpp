// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab4GameModeBase.h"
#include "Lab4PlayerController.h"
#include "Lab4Character.h"

ALab4GameModeBase::ALab4GameModeBase()
{
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(
		TEXT("Class'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));

	if (pawnBPClass.Object)
	{
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		DefaultPawnClass = ALab4Character::StaticClass();
	}

	PlayerControllerClass = ALab4PlayerController::StaticClass();
}
