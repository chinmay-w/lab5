// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab4Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8 {
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UCLASS()
class LAB4_API ALab4Character : public ACharacter
{
	GENERATED_BODY()

public:
	//Lab 5
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
	FORCEINLINE class USpringArmComponent* GetSpringArmComponent() const { return SpringArmComponent; }

	// Sets default values for this character's properties
	ALab4Character();
	UFUNCTION(BlueprintImplementableEvent)
		void MoveForward(float value);
	UFUNCTION(BlueprintImplementableEvent)
		void MoveRight(float value);
	UFUNCTION(BlueprintImplementableEvent)
		void JumpStarted();
	UFUNCTION(BlueprintImplementableEvent)
		void InteractStarted();
	UFUNCTION(BlueprintImplementableEvent)
		void InteractEnded();
	UFUNCTION(BlueprintCallable)
		bool CanPerformAction(ECharacterActionStateEnum updatedAction);
	UFUNCTION(BlueprintCallable)
		void ApplyMovementForward(float scale);
	UFUNCTION(BlueprintCallable)
		void ApplyMovementRight(float scale);
	UFUNCTION(BlueprintCallable)
		void BeginInteract();
	UFUNCTION(BlueprintCallable)
		void EndInteract();
	UFUNCTION(BlueprintCallable)
		void UpdateActionState(ECharacterActionStateEnum newAction);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ECharacterActionStateEnum CharacterActionState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float interactionLength;

	//LAB 5
	UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
		class UCameraComponent* CameraComponent;
	UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
		class USpringArmComponent* SpringArmComponent;






protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	FTimerHandle InteractionTimerHandle;

};
