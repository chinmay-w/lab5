// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4PlayerController.h"
#include "Lab4Character.h"

ALab4PlayerController::ALab4PlayerController() {

}

void ALab4PlayerController::SetupInputComponent() {
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &ALab4PlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ALab4PlayerController::MoveRight);
	InputComponent->BindAction("Interact", IE_Pressed, this, &ALab4PlayerController::Interact);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ALab4PlayerController::Jump);
}
	
void ALab4PlayerController::MoveForward(float value) {
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->MoveForward(value);
	}
}

void ALab4PlayerController::MoveRight(float value) {
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->MoveRight(value);
	}
}

void ALab4PlayerController::Interact() {
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->InteractStarted();
	}
}

void ALab4PlayerController::Jump() {
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->JumpStarted();
	}
}